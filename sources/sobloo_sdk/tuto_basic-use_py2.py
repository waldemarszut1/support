#tuto_basic-use_py2
#1 Eodag basic use tutorial
#This notebook presents the two basic features of eodag : search and download.
#In [1]: 
import os, fnmatch
import json, geojson
from datetime import date

import ipyleaflet as ipyl
import ipywidgets as ipyw
from IPython.display import display, Image

from eodag.api.core import EODataAccessGateway

base_dir = '/projects/sdk'
#The first step is to initialize the session by creating an eodag instance, with the configuration file that contains all providers credentials (here called myconf.yml):


#In [2]: 
dag = EODataAccessGateway(user_conf_file_path='%s/myconf.yml' % base_dir)
#Here is the list of availables product types that can be searched, with additionnal informations :
#We make a search on L2A Sentinel product types in Southern France, setting airbus-ds as preferred provider:


#In [4]: 
product_type = 'S2_MSI_L1C'
extent = {
	'lonmin': -1.999512,
	'lonmax': 4.570313,
	'latmin': 42.763146,
	'latmax': 46.754917
}
dag.set_preferred_provider(provider='airbus-ds')

products = dag.search(product_type, startTimeFromAscendingNode='2018-06-01', completionTimeFromAscendingNode=date.today().isoformat(), geometry=extent)

dag.serialize(products, filename='%s/search_results.geojson' % base_dir)


#Out[4]: 
'/projects/sdk/search_results.geojson'
#The result of the search is easily saved in a geojson file, and we can check extents before downloading products with ipyleaflet API :


#In [5]: 
emap, label = ipyl.Map(center=[43.6, 1.5], zoom=4), ipyw.Label(layout=ipyw.Layout(width='100%'))
layer = ipyl.GeoJSON(data=products.as_geojson_object(), hover_style={'fillColor': 'yellow'})

def hover_handler(event=None, id=None, properties=None):
	label.value = properties['title']

layer.on_hover(hover_handler)
emap.add_layer(layer)

ipyw.VBox([emap, label])

#We can download both from the eodag SearchResult object products, or from the GeoJson previously created :
#In [6]: 
product = products[0]
product_path = product.download()

#No handlers could be found for logger "eodag.plugins.download.http"
#Extracting files from /home/baptiste/data/SENTINEL2A_20180607-104022-457_L2A_T31TDL_D.zip: 100%|*********| 44/44 [01:18<00:00, 1.79s/file]
#Finally we can have a look on the RGB quicklook provided in one product archive.

#In [7]:
for dName, sdName, fList in os.walk(product_path):
	for fileName in fList:
		if fnmatch.fnmatch(fileName, '*.jpg'):
			display(Image(os.path.join(dName, fileName), width=500))
