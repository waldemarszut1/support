sobloo SDK basics tutorial
==

This tutorial presents the 2 basic features of sobloo SDK: search & download data

Search for data
--
```
import os, fnmatch 
import json, geojson 
from datetime import date
import ipyleaflet as ipyl 
import ipywidgets as ipyw 
from IPython.display import display, Image
from eodag.api.core import EODataAccessGateway
base_dir = '/projects/sdk'
```

The ﬁrst step is to initialize the session by creating an eodag instance, 
with the conﬁguration ﬁle that contains all providers credentials (here called myconf.yml):

```
dag = EODataAccessGateway(user_conf_file_path='%s/myconf.yml' % base_dir)
```

Here is the list of availables product types that can be searched, with additionnal informations:
We make a search on L2A Sentinel product types in Southern France, setting airbus-ds as preferred provider:

```
product_type = 'S2_MSI_L1C' 
 
 extent = { 'lonmin': -1.999512,
 'lonmax': 4.570313,
 'latmin': 42.763146,
 'latmax': 46.754917
 } 
 
 dag.set_preferred_provider(provider='airbus-ds')
 
 products = dag.search(product_type,
 startTimeFromAscendingNode='2018-06-01',
 completionTimeFromAscendingNode=date.today().isoformat(),
 geometry=extent)
 
 dag.serialize(products, filename='%s/search_results.geojson' % base_dir)
```
Output: '/projects/sdk/search_results.geojson'


The result of the search is easily saved in a geojson ﬁle, and we can check extents before downloading products with ipyleaﬂet API :
```
 emap, label = ipyl.Map(center=[43.6, 1.5], zoom=4), ipyw.Label(layout=ipyw.Layout(width='100%'))
 layer = ipyl.GeoJSON(data=products.as_geojson_object(), hover_style={'fillColor': 'yellow'})
 
 def hover_handler(event=None, id=None, properties=None):
  label.value = properties['title']
  layer.on_hover(hover_handler) 
  emap.add_layer(layer)
  ipyw.VBox([emap, label])
```

![mymig](/sources/sobloo_sdk/IMG_01.png)

Download data
--
We can download both from the eodag SearchResult object products, or from the GeoJson previously created :

```
 product = products[0] 
 product_path = product.download()
```

No handlers could be found for logger "eodag.plugins.download.http" 
Extracting files from /home/baptiste/data/SENTINEL2A_20180607-104022-457_L2A_T31TDL_D.zip: 100%|*********| 44/44 [01:18<00:00, 1.79s/file]

```
for dName, sdName, fList in os.walk(product_path): 
  for fileName in fList: if fnmatch.fnmatch(fileName, '*.jpg'): 
    display(Image(os.path.join(dName, fileName), width=500))
```

![mymig](/sources/sobloo_sdk/IMG_02.png)

