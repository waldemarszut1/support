variable "username" {}
variable "password" {}
variable "domain_id" {}
variable "tenant_id" {}
variable "endpoint" {}
variable "region" {}

variable "key_pair" {}
variable "network_id" {}
variable "subnet_id" {}
variable "security_group_id" {}

variable "myvariable_action_a" {}
variable "myvariable_action_b" {}