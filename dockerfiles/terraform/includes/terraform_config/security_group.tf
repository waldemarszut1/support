module "ecs_cluster_sg" {
  source = "../modules/terraform-fe-securitygroup"

  name        = "${var.security_group_name}"
  description = "Security group for ecs_cluster instances"

  ingress_with_source_cidr = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      ethertype   = "IPv4"
      source_cidr = "0.0.0.0/0"
    }
  ]
}
