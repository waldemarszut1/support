module "ecs_cluster" {
  source = "../modules/terraform-fe-ecs"

  instance_name  = "${var.instance_name}"
  instance_count = "${var.instance_count}"
  availability_zone = "${var.availability_zone}"

  sysvol_size = "${var.sysvol_size}"

  image_id           = "${var.image_id}"
  flavor_name        = "${var.flavor_name}"
  key_name           = "${var.key_name}"
  security_groups    = ["${module.ecs_cluster_sg.security_group_id}"]
  subnet_id          = "${flexibleengine_networking_subnet_v2.subnet.id}"
  network_id         = "${flexibleengine_networking_network_v2.network.id}"

  #user_data          = "${data.template_cloudinit_config.config.rendered}"

  attach_eip = true

  metadata = {
    Terraform = "true"
    Environment = "hackathon"
  }
}

resource "flexibleengine_blockstorage_volume_v2" "datavol" {
  availability_zone = "${var.availability_zone}"
  count             = "${var.instance_count}"
  name              = "${var.instance_name}-${count.index+1}-datavol"
  size              = "${var.datavol_size}"
  volume_type       = "${var.datavol_type}"
}

resource "flexibleengine_compute_volume_attach_v2" "datavol_attach" {
  count       = "${var.instance_count}"
  instance_id = "${module.ecs_cluster.id[count.index]}"
  volume_id   = "${flexibleengine_blockstorage_volume_v2.datavol.*.id[count.index]}"
}
