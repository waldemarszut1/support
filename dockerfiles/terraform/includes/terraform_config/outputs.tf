output "name" {
    description = "List of names the created servers"
    value       = "${module.ecs_cluster.name}"
}

output "public_ip" {
    description = "List of public floating ip addresses of the created servers"
    value       = "${module.ecs_cluster.public_ip}"
}
