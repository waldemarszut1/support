S3FS
==

This docker allows mounting any Orange S3 bucket in its /share/s3 directory.
To run it, simply build or get the image from the registry and run the script run-docker.sh

Before running it, you need to edit the parameters:
- mybucket: name of the bucket you want to access
- myid: you access id
- mykey: your access key

Enjoy.
