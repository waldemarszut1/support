#sobloo SDK configuration

Installation
--
To install the sobloo SDK, just issue the following commands in a shell:
```
pip install eodag
```

User configuration
--
The configuration on user side resides in the file eodagconf.yml
The file describes:
* where files are downloaded 'outputs_prefix'
* the API KEY to use for protected APIs (download)

The file content is as follows:
```
outputs_prefix: '/shared/downloads'
extract: 'true'

airbus-ds:
    credentials:
        apikey: 'YOUR_API_KEY_SECRET'
```

Server configuration
--
All the configuration is contained in the file 'providers.yml'. The file must 
be placed in the directory:
_/usr/local/lib/python[version]/dist-packages/eodag/resources/_

You must update the parameter _Authorization: "Apikey `your_api_key_secret`"_ 
with your API KEY secret.
